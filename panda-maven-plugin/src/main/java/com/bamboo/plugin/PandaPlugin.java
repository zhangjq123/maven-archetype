package com.bamboo.plugin;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "panda")
public class PandaPlugin extends AbstractMojo {
    @Parameter
    String sex;
    @Parameter
    String describe;
    public void execute() throws MojoExecutionException, MojoFailureException {
        getLog().info(String.format("panda sex=%s,describe=%s", sex, describe));
    }
}