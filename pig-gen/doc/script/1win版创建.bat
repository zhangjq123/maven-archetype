mvn archetype:generate ^
       -DgroupId=com.pig4cloud ^
       -DartifactId=pig-demo ^
       -Dversion=1.0.0-SNAPSHOT ^
       -Dpackage=com.pig4cloud.pig.demo ^
       -DarchetypeGroupId=com.pig4cloud.archetype ^
       -DarchetypeArtifactId=pig-gen ^
       -DarchetypeVersion=2.4.2 ^
       -DarchetypeCatalog=local
